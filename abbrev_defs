;;-*-coding: utf-8;-*-
(define-abbrev-table 'Buffer-menu-mode-abbrev-table '())

(define-abbrev-table 'bibtex-mode-abbrev-table '())

(define-abbrev-table 'calendar-mode-abbrev-table '())

(define-abbrev-table 'clojure-mode-abbrev-table '())

(define-abbrev-table 'comint-mode-abbrev-table '())

(define-abbrev-table 'completion-list-mode-abbrev-table '())

(define-abbrev-table 'direx:direx-mode-abbrev-table '())

(define-abbrev-table 'dsssl-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-byte-code-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-mode-abbrev-table '())

(define-abbrev-table 'fundamental-mode-abbrev-table '())

(define-abbrev-table 'global-abbrev-table '())

(define-abbrev-table 'help-mode-abbrev-table '())

(define-abbrev-table 'inferior-lisp-mode-abbrev-table '())

(define-abbrev-table 'inferior-python-mode-abbrev-table '())

(define-abbrev-table 'inferior-scheme-mode-abbrev-table '())

(define-abbrev-table 'lisp-mode-abbrev-table '())

(define-abbrev-table 'nrepl-connections-buffer-mode-abbrev-table '())

(define-abbrev-table 'oberon-mode-abbrev-table
  '(
    ("array" "ARRAY" indent-according-to-mode 2)
    ("begin" "BEGIN" indent-according-to-mode 1)
    ("boolean" "BOOLEAN" indent-according-to-mode 0)
    ("by" "BY" indent-according-to-mode 0)
    ("case" "CASE" indent-according-to-mode 0)
    ("char" "CHAR" indent-according-to-mode 0)
    ("const" "CONST" indent-according-to-mode 0)
    ("div" "DIV" indent-according-to-mode 1)
    ("do" "DO" indent-according-to-mode 5)
    ("else" "ELSE" indent-according-to-mode 0)
    ("elsif" "ELSIF" indent-according-to-mode 0)
    ("end" "END" indent-according-to-mode 4)
    ("exit" "EXIT" indent-according-to-mode 0)
    ("false" "FALSE" indent-according-to-mode 0)
    ("for" "FOR" indent-according-to-mode 4)
    ("if" "IF" indent-according-to-mode 1)
    ("import" "IMPORT" indent-according-to-mode 0)
    ("in" "IN" indent-according-to-mode 0)
    ("integer" "INTEGER" indent-according-to-mode 4)
    ("is" "IS" indent-according-to-mode 0)
    ("longint" "LONGINT" indent-according-to-mode 0)
    ("longreal" "LONGREAL" indent-according-to-mode 0)
    ("loop" "LOOP" indent-according-to-mode 0)
    ("mod" "MOD" indent-according-to-mode 1)
    ("module" "MODULE" indent-according-to-mode 0)
    ("nil" "NIL" indent-according-to-mode 0)
    ("of" "OF" indent-according-to-mode 2)
    ("or" "OR" indent-according-to-mode 1)
    ("pointer" "POINTER" indent-according-to-mode 0)
    ("procedure" "PROCEDURE" indent-according-to-mode 1)
    ("real" "REAL" indent-according-to-mode 0)
    ("record" "RECORD" indent-according-to-mode 0)
    ("repeat" "REPEAT" indent-according-to-mode 0)
    ("return" "RETURN" indent-according-to-mode 0)
    ("set" "SET" indent-according-to-mode 0)
    ("shortint" "SHORTINT" indent-according-to-mode 0)
    ("then" "THEN" indent-according-to-mode 1)
    ("to" "TO" indent-according-to-mode 4)
    ("true" "TRUE" indent-according-to-mode 0)
    ("type" "TYPE" indent-according-to-mode 0)
    ("until" "UNTIL" indent-according-to-mode 0)
    ("var" "VAR" indent-according-to-mode 3)
    ("while" "WHILE" indent-according-to-mode 1)
    ("with" "WITH" indent-according-to-mode 0)
   ))

(define-abbrev-table 'occur-edit-mode-abbrev-table '())

(define-abbrev-table 'occur-mode-abbrev-table '())

(define-abbrev-table 'org-mode-abbrev-table '())

(define-abbrev-table 'outline-mode-abbrev-table '())

(define-abbrev-table 'package-menu-mode-abbrev-table '())

(define-abbrev-table 'process-menu-mode-abbrev-table '())

(define-abbrev-table 'prog-mode-abbrev-table '())

(define-abbrev-table 'python-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'restclient-mode-abbrev-table '())

(define-abbrev-table 'scheme-mode-abbrev-table '())

(define-abbrev-table 'select-tags-table-mode-abbrev-table '())

(define-abbrev-table 'shell-mode-abbrev-table '())

(define-abbrev-table 'special-mode-abbrev-table '())

(define-abbrev-table 'tabulated-list-mode-abbrev-table '())

(define-abbrev-table 'text-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-edit-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-view-mode-abbrev-table '())

