;;;;;;; Packages ;;;;;;;
(require 'package)

;; Add  MELPA repo
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;; Installed selected packages if they are not installed already
(defvar my-packages '(golden-ratio
                      smartparens
                      smex
                      ido-ubiquitous
                      solarized-theme
                      magit
                      auto-complete
                      clojure-mode
                      clojure-test-mode
                      cider)
  "A list of packages to ensure are installed at launch.")

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))


;;;;;;; UX ;;;;;;;
;; Disable toolbar if emacs is opened in graphics mode
(when (display-graphic-p)
      (tool-bar-mode -1))

(load-theme 'solarized-light t)

;(set-default 'cursor-type 'bar)

;; Highlight the current line
(global-hl-line-mode 1)

; Highlight text selection
(transient-mark-mode 1)

; Delete selected text when typing
(delete-selection-mode 1)

; Highlight the entire bracketed expression
;(setq show-paren-style 'expression)

(show-paren-mode 1)

;(global-linum-mode 1)

;; Enable auto-complete
(require 'auto-complete-config)
(ac-config-default)

;; Global auto revert mode causes emacs to automatically
;; reload files changed outside of emacs.
(global-auto-revert-mode 1)

;; Enable ido
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
(ido-ubiquitous t)

;; Enable Smex
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
; This is th old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; Make ediff spilt window horizontally
(setq ediff-split-window-function 'split-window-horizontally)

;; Direx mode
(require 'direx)
(global-set-key (kbd "C-x C-y") 'direx:jump-to-directory)
(global-set-key (kbd "C-x C-j") 'direx-project:jump-to-project-root)

;; Enable Smartparens
(smartparens-global-mode 1)

;;;;;;; Key Bindings ;;;;;;;
;; Mac OS X key remapping
(setq mac-option-modifier 'super)
(setq mac-command-modifier 'meta)

;; Use default winmove keybindings
;; shift + arrow keys to move to other windows
;(windmove-default-keybindings)

(global-set-key (kbd "M-/") 'hippie-expand)

(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

;;;;;;; Clojure ;;;;;;;
(setq nrepl-hide-special-buffers t)

(require 'ac-nrepl)
(add-hook 'nrepl-mode-hook 'ac-nrepl-setup)
(add-hook 'nrepl-interaction-mode-hook 'ac-nrepl-setup)
(eval-after-load "auto-complete"
 '(add-to-list 'ac-modes 'nrepl-mode))


;;;;;;; Paths ;;;;;;;
(setq default-directory "~/")

;; Oberon mode file load path
(add-to-list 'load-path "~/.emacs.d/")
(add-to-list 'load-path "~/.emacs.d/oberon/")


;; Set the exec-path of emacs equal to the shell's $PATH,
;; so that emacs knows the shell's $PATH
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell 
	 (replace-regexp-in-string "[[:space:]\n]*$" "" 
				   (shell-command-to-string "$SHELL -l -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))


;;;;;;; Indentation and Tabs ;;;;;;;
;; Insert 4 spaces when the tab key is hit for C family languages.
(setq c-basic-offset 4)

;; Display the tab-character as a 4 column indentation.
(setq tab-width 4)

;; Use spaces instead of tabs.
(setq indent-tabs-mode nil)


;; Enable automatic saving of emacs session
(desktop-save-mode 1)

;; Enable autofill mode
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Depencency for some functions
(require 'cl)


;;;;;;; Oberon ;;;;;;;
(add-to-list 'auto-mode-alist '("\\.mod\\'" . oberon-mode))
(autoload 'oberon-mode "oberon" nil t)
(add-hook 'oberon-mode-hook (lambda () (abbrev-mode t)))

;;;;;;; C/C++ ;;;;;;;
;; Use C-c o to switch between header and implementation files of C/C++
(add-hook 'c-mode-common-hook
  (lambda()
    (local-set-key (kbd "C-c o") 'ff-find-other-file)))


;; No prompt for M-x compile
;(setq compilation-read-command nil)


;;;;;;; Octave ;;;;;;;
;(autoload 'octave-mode "octave-mod" nil t)
;(setq auto-mode-alist 
;      (cons '("\\.m$" . octave-mode) auto-mode-alist))

;;;;;;; Javascript ;;;;;;;

;; Make emacs use js2-mode as the default mode for javascript.
(add-to-list 'auto-mode-alist (cons (rx ".js" eos) 'js2-mode))


;;;;;;; Scheme ;;;;;;;
;; Copied from http://www.yinwang.org/
;; All rights to Yin Wang
(require 'iuscheme)
(setq scheme-program-name "petite")


;; bypass the interactive question and start the default interpreter
(defun scheme-proc ()
  "Return the current Scheme process, starting one if necessary."
  (unless (and scheme-buffer
               (get-buffer scheme-buffer)
               (comint-check-proc scheme-buffer))
    (save-window-excursion
      (run-scheme scheme-program-name)))
  (or (scheme-get-process)
      (error "No current process. See variable `scheme-buffer'")))


(defun scheme-split-window ()
  (cond
   ((= 1 (count-windows))
    (delete-other-windows)
    (split-window-vertically (floor (* 0.68 (window-height))))
    (other-window 1)
    (switch-to-buffer "*scheme*")
    (other-window 1))
   ((not (find "*scheme*"
               (mapcar (lambda (w) (buffer-name (window-buffer w)))
                       (window-list))
               :test 'equal))
    (other-window 1)
    (switch-to-buffer "*scheme*")
    (other-window -1))))


(defun scheme-send-last-sexp-split-window ()
  (interactive)
  (scheme-split-window)
  (scheme-send-last-sexp))


(defun scheme-send-definition-split-window ()
  (interactive)
  (scheme-split-window)
  (scheme-send-definition))

(add-hook 'scheme-mode-hook
  (lambda ()
    (define-key scheme-mode-map (kbd "<f5>") 'scheme-send-last-sexp-split-window)
    (define-key scheme-mode-map (kbd "<f6>") 'scheme-send-definition-split-window)))


;;;;;;; Miscellaneous ;;;;;;;
;; Auto-indent new lines
(define-key global-map (kbd "RET") 'newline-and-indent)

;; Auto-indent yanked (pasted) code
(dolist (command '(yank yank-pop))
   (eval `(defadvice ,command (after indent-region activate)
            (and (not current-prefix-arg)
                 (member major-mode '(emacs-lisp-mode
				      lisp-mode
				      clojure-mode
				      scheme-mode
				      haskell-mode
				      ruby-mode
				      rspec-mode
				      python-mode
				      c-mode
				      c++-mode
				      objc-mode
				      latex-mode
				      plain-tex-mode))
                 (let ((mark-even-if-inactive transient-mark-mode))
                   (indent-region (region-beginning) (region-end) nil))))))

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

(require 'table)

(require 'saveplace)
(setq-default save-place t)

(setq x-select-enable-clipboard t
      x-select-enable-primary t
      save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t
      save-place-file (concat user-emacs-directory "places")
      backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))

;; Enable Flycheck in modes that it can check the syntax
;(add-hook 'after-init-hook #'global-flycheck-mode)


;; Make html-mode use 4-space indentation
(add-hook 'html-mode-hook
	  (lambda ()
	    (setq sgml-basic-offset 4)))


;; Disable auto-compilation of SCSS mode
(setq scss-compile-at-save nil)
